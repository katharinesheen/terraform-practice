provider "aws" {
    region = "ap-south-1" 
    access_key = "AKIA2HGPF37UINGC7LNL"
    secret_key = "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"

}

resource "aws_vpc" "mydemo-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "mydemo-subnet-1"{
    vpc_id = aws_vpc.mydemo-vpc.id
    cidr_block = var.subnet_cidr_block
     availability_zone = var.avail_zone
     tags = {
         Name: "${var.env_prefix}-subnet-1"
     }
}

resource "aws_internet_gateway" "demo-igw"{
    vpc_id = aws_vpc.mydemo-vpc.vpc_id
    tags = {
        Name: "${var.env_prefix}-igw"

    }
}

resource "aws_default_route_table" "main-rtb"{
    default_route_table_id = aws_vpc.mydemo-vpc.aws_default_route_table_id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.main-rtb.vpc_id
    }
    tags = {
        Name: "${var.env_prefix}-main-rtb"
    }

}

resource "aws_security_group" "demo-sg"{
    name = "demo-sg"
    vpc_id = aws_vpc.mydemo-vpc.vpc_id

    ingress{
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]

    }

    egress{
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = []
    }
    tags  = {
        Name: "${var.env_prefix}-sg"
    }
}